#  [cammaratafg.gitlab.io](https://cammaratafg.gitlab.io)

CV usando [jsonresume.org](https://jsonresume.org/) y CI/CD para publicar las actualizaciones en Gitlab pages.


## Serve
  npm i
  npm run resume serve (-- --theme elegant)


## Exportar CV
  ### PDF
  `npm run resume export cv.pdf`

  ### HTML
  `npm run resume export cv.html`


